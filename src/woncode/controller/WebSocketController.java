package woncode.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.RemoteEndpoint;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint(value="/ReadyUpload")
public class WebSocketController {
	private RemoteEndpoint.Basic remoteEndpoint;
	@OnOpen
	public void onOpen(Session session, EndpointConfig conf) throws IOException, InstantiationException{
		remoteEndpoint=session.getBasicRemote();
		UtilContext.fileID++;
		ArrayList<Object> fileContextList=new ArrayList<Object>();
		fileContextList.add(this);
		UtilContext.fileContextMap.put(UtilContext.fileID, fileContextList);
		remoteEndpoint.sendText(""+UtilContext.fileID);
		System.out.println("WebSocket open:fileID="+UtilContext.fileID);
	}
	
	@OnMessage
	public void onMessage(Session session, String msg){
		System.out.println("OnMessage:"+msg);
	}
	
	@OnClose
	public void onClose(){
		System.out.println("WebSocketController close");
	}
	
	public void sendStartUpload() throws IOException{
		this.remoteEndpoint.sendText("StartUpload");
	}
}
